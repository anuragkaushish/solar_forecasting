# -*- coding: utf-8 -*-
"""
Created on Wed May 30 13:02:45 2018

@author: Anurag
"""

import pandas as pd
import pymysql
import datetime as dt

d = pd.read_table("D:/climate connect/data/Hindustan Power/s5697562.txt", sep="," ) 

d['local_datetime'] = pd.to_datetime(d.Date + ' ' + d.Time, format='%m/%d/%Y %H:%M:%S')- dt.timedelta(minutes=15)
d= d[:-1].copy()
d['datetime']= d['local_datetime'].apply(lambda x: x.tz_localize('Asia/Kolkata').tz_convert('UTC').tz_localize(None))

d= d.rename(columns={'Zenith (refracted)':'zenith','Azimuth angle':'azimuthal','ETR tilt':'etr'})

d['plant_id']=1

#convert datetime  into string
#d['local_datetime']=d['local_datetime'].apply(lambda x: dt.datetime.strftime(x,'%Y-%m-%d %H:%M:%S'))
#d['datetime']=d['datetime'].apply(lambda x: dt.datetime.strftime(x,'%Y-%m-%d %H:%M:%S'))
#d[["datetime","zenith", "azimuthal", "etr","local_datetime"]] = d[["datetime","zenith", "azimuthal", "etr","local_datetime"]].astype(str) 

d= d[['plant_id','datetime','zenith','azimuthal','etr','local_datetime']]

d.sort_values('datetime',inplace=True)
d.reset_index(drop=True,inplace=True)

from sqlalchemy.engine import create_engine

engine= create_engine("mysql+pymysql://{0}:{1}@{2}:3306/{3}".format('root','dentintheuniverse','vps2.climate-connect.com','Hindustan_Power'),echo= False) 
d.to_sql(name= 'solar_parameter',con= engine,if_exists= 'append',index= False)
engine.dispose()


#### to upload solar parameters into database 
import requests
import json

def get_token(user,password):
    payload= '{"username":"'+user+'","password":"'+password+'"}'
    headers= {'content-type': "application/json"}
    response= requests.post("http://testapi.climate-connect.com/api/get-token",data = payload,headers=headers).json()
    return(response['access_token'])

def get_clients(token):
    header= {"token":token ,"content-type":"application/json"}
    response= requests.post("http://testapi.climate-connect.com/api/Prism/getClients",headers=header).json()['data']
    temp= pd.DataFrame(response)
    return(temp)
     
def get_plants(token,client_id):
    header= {"token":token ,"content-type":"application/json"}
    response= requests.post("http://testapi.climate-connect.com/api/solar/getPlant?type=plant&client_id=%s" % client_id,headers=header).json()['data']
    temp= pd.DataFrame(response)
    temp= temp[['plantId','name','capacity','latitude','longitude','tilt','azimuth']]
    return(temp)

def write_df_api(temp,client_id,token,url):
    for i in range(len(temp.columns)):
        if temp[temp.columns[i]].dtypes=='<M8[ns]':
            temp[temp.columns[i]]= temp[temp.columns[i]].apply(lambda x: dt.datetime.strftime(x,'%Y-%m-%d %H:%M:%S'))

    temp= temp.to_json(orient='records')
    
    payload= {'client_id': client_id, 'data': temp}
    headers = {'content-type': "application/json",
               'token': token
               }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    return(response.text)

user= "eds@climate-connect.com"
password= "***********"
token= get_token(user,password)
clients= get_clients(token)
client_id= 27  ### adani
plants_info= get_plants(token,client_id)
 
url = "http://testapi.climate-connect.com/api/solar/setData"

print(write_df_api(d.copy(),client_id,token,url))
