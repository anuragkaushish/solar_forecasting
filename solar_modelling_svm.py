# -*- coding: utf-8 -*-
"""
Created on Thu May 10 11:34:50 2018

@author: Anurag
"""

import pandas as pd
import datetime as dt
from sklearn.model_selection import train_test_split
from sklearn import linear_model
# import csv data files for input
df= pd.read_csv("C:/Users/Anurag/Desktop/solar data/solar_generation_new.csv")
df1=pd.read_csv("C:/Users/Anurag/Desktop/solar data/solar_parameter.csv")
df2=pd.read_csv("C:/Users/Anurag/Desktop/solar data/weather_io_data.csv")
df3=pd.read_csv("C:/Users/Anurag/Desktop/solar data/Weather_Station_Data_new.csv")


#dft=df['plantID']
# merge dataframes on common columns
dft=pd.merge(df,df2, on=['date','time'])
df1=df1.rename(index=str, columns={"Time": "time", "Date": "date"})
dft=pd.merge(dft,df1, on=['date','time'])
dft=pd.merge(dft,df3, on=['date','time'])

dftn=dft.loc[:,['datetime_data','ghi','ghi_inclined','cloudCover','temperature','dewPoint', 'humidity', 'pressure','Zenith', 'Azimuth','ETR','generation']]
#dftn['datetime_data']=dftn.loc[:,'datetime_data'].apply(lambda x: dt.datetime.strptime(x,'%Y-%m-%d %H:%M:%S'))

dftn['datetime_data']=pd.to_datetime(dftn['datetime_data'])


dftrain=dftn.loc[dftn['datetime_data'].dt.date <= dt.date(2018,2,28)]
x_train=dftrain[[ 'cloudCover','temperature','dewPoint', 'humidity', 'pressure', 'Zenith', 'Azimuth', 'ETR']]
y_train=dftrain['generation']
dftest=dftn.loc[dftn.datetime_data.dt.date >dt.date(2018,2,28)]
x_test=dftest[['cloudCover', 'temperature', 'dewPoint', 'humidity', 'pressure', 'Zenith', 'Azimuth', 'ETR']]
y_test=dftest['generation']

# Create linear regression object
linear = linear_model.LinearRegression()
# Train the model using the training sets and check score
linear.fit(x_train, y_train)
linear.score(x_train, y_train)
#Equation coefficient and Intercept
print('Coefficient: \n', linear.coef_)
print('Intercept: \n', linear.intercept_)
#Predict Output
predicted= linear.predict(x_test)

dts= pd.DataFrame({'actual':y_test,'predicted':predicted})
dts['error']=abs(dts['actual']-dts['predicted'])
dts['acc']=(1-dts['error']/30)*100
m=dts['acc'].mean()



########################support vector machine

#Import Library
from sklearn import svm
from sklearn.svm import SVR
model = SVR(kernel='linear', C=1)
model= SVR(kernel='rbf', C=1, gamma=0.1)
model = SVR(kernel='poly', C=1, degree=3)

model.fit(x_train, y_train)
model.score(x_train, y_train)
#Predict Output
predictedsvm= model.predict(x_test)

dtsvm= pd.DataFrame({'actual':y_test,'predicted':predictedsvm})
dtsvm['error']=abs(dtsvm['actual']-dtsvm['predicted'])
dtsvm['acc']=(1-dtsvm['error']/30)*100
#svr linear
msvm=dtsvm['acc'].mean()
# rbf
msvmrbf=dtsvm['acc'].mean()
# poly
msvmpoly=dtsvm['acc'].mean()




