# -*- coding: utf-8 -*-
"""
Created on Sun May 13 14:41:50 2018

@author: Anurag
"""

import pandas as pd
import datetime as dt

import os

os.chdir('D://climate connect//internship tasks//10th may, modelling in svm and linear regression/solar data/')
# read csv files
df=pd.read_csv('solar_generation_new.csv')
df1=pd.read_csv('solar_parameter.csv')
df2=pd.read_csv('weather_io_data.csv')
df3=pd.read_csv('Weather_Station_Data_new.csv')

# change to datetime (object)
df['datetime'] = pd.to_datetime(df.date + df.time, format='%Y-%m-%d%H:%M:%S')
df1['datetime'] = pd.to_datetime(df1.Date + df1.Time, format='%Y-%m-%d%H:%M:%S')
df1= df1[df1.datetime.dt.date<= dt.date(2018,3,15)]
df2['datetime'] = pd.to_datetime(df2.date + df2.time, format='%Y-%m-%d%H:%M:%S')
df3=df3.rename(index=str, columns={"datetime_data": "datetime"})
df3['datetime']= pd.to_datetime(df3['datetime'])


#merge the following dataframes on the basis of datetime
dff=df.merge(df1, on='datetime',how='outer')
dff=dff.merge(df2, on='datetime',how='outer')
dff=dff.merge(df3, on='datetime',how='outer')

# select the crucial parameters
dfff=dff[['datetime','temperature','dewPoint','humidity','cloudCover','pressure','Zenith','Azimuth','ETR','generation']]

# fill the missing values
dffff=dfff.interpolate()
dffff=dffff.fillna(method='bfill')

#dffff.to_csv('D:/climate connect/internship tasks/new.csv')


#split dataframe in test and train
split_date = pd.datetime(2018,2,28)

train = dffff.loc[dffff['datetime'] <= split_date]
test = dffff.loc[dffff['datetime'] > split_date]

x_train =train[['temperature', 'dewPoint', 'humidity', 'cloudCover','pressure','Zenith', 'Azimuth', 'ETR']]
x_test= test[['temperature', 'dewPoint', 'humidity', 'cloudCover','pressure','Zenith', 'Azimuth', 'ETR']]


y_train=train['generation']
y_test=test['generation']


import xgboost as xgb
model = xgb.XGBRegressor()
model.fit(x_train,y_train)

predicted= model.predict(x_test)


dtxb= pd.DataFrame({'actual':y_test,'predictions':predicted})
dtxb.loc[dtxb.predictions<0,'predictions']=0
dtxb['error']=abs(dtxb['actual']-dtxb['predictions'])
dtxb['acc']=(1-dtxb['error']/30)*100
dtxb= dtxb[dtxb.actual>0]
mxb=dtxb['acc'].mean()
# acc_xgboost_default= 91.38%
dtxb.to_csv('D:/climate connect/XGboost_report.csv')

### tuning xgboost

import xgboost as xgb

model = xgb.XGBRegressor(learning_rate =0.08,n_estimators=50, max_depth=8,
 subsample=0.8,
 colsample_bytree=0.5,)
model.fit(x_train,y_train)

predictedtune= model.predict(x_test)


dtxb= pd.DataFrame({'actual':y_test,'predictions':predictedtune})
dtxb.loc[dtxb.predictions<0,'predictions']=0
dtxb['error']=abs(dtxb['actual']-dtxb['predictions']) 
dtxb['acc']=(1-dtxb['error']/30)*100
dtxb= dtxb[dtxb.actual>0]
mxb=dtxb['acc'].mean()

# acc_xgboost_tuned=93.32%
dtxb.to_csv('D:/climate connect/XGboost_report_tuning_parameters.csv')
