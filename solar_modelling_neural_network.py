# -*- coding: utf-8 -*-
"""
Created on Wed May 16 11:47:05 2018

@author: Anurag
"""

import pandas as pd
import datetime as dt

import os

os.chdir('D:/climate connect/internship tasks/10th may, modelling in svm and linear regression/solar_data_2')
capacity=30
# read csv files
df=pd.read_csv('solar_generation_new.csv')
df1=pd.read_csv('solar_parameter.csv')
df2=pd.read_csv('weather_io_data.csv')
df3=pd.read_csv('Weather_Station_Data_new.csv')

#drop dates with NAN value
a = ['0000-00-00']
df1= df1[~df1['Date'].isin(a)]

# change to datetime (object)
df['datetime'] = pd.to_datetime(df.date + df.time, format='%Y-%m-%d%H:%M:%S')
df1['datetime'] = pd.to_datetime(df1.Date + df1.Time, format='%Y-%m-%d%H:%M:%S')
df1= df1[df1.datetime.dt.date<= dt.date(2018,5,10)]
df2['datetime'] = pd.to_datetime(df2.date + df2.time, format='%Y-%m-%d%H:%M:%S')
df3=df3.rename(index=str, columns={"datetime_data": "datetime"})
df3['datetime']= pd.to_datetime(df3['datetime'])

df.loc[df.generation<0,'generation']=0
df=df[df.generation<=capacity]

df['datetime']= df['datetime'].dt.floor('15min')        
df1['datetime']= df1['datetime'].dt.floor('15min')        
df2['datetime']= df2['datetime'].dt.floor('15min')
df3['datetime']= df3['datetime'].dt.floor('15min')  

df=df.groupby('datetime',as_index=False)['generation'].mean()
df3=df3.groupby('datetime',as_index=False)['ghi','ghi_inclined'].mean()
df = df.set_index('datetime')
df = df.resample('15T').asfreq()
df = df.reset_index()
df['time_block']= df['datetime'].apply(lambda x: ((x.hour*60+x.minute)/15+1))

   
df_new = pd.DataFrame()
for m in range(1,97):
     tmp = df[df['time_block'] == m]
     tmp = tmp.sort_values(by='datetime') 
     tmp = tmp.interpolate(method="linear")
     df_new = pd.concat([df_new, tmp], axis=0)
     
df_new = df_new.sort_values(by='datetime') 
dff=df_new.merge(df1, on='datetime',how='left')
dff=dff.merge(df2, on='datetime',how='left')
         
df5=dff[['datetime','temperature','dewPoint','humidity','cloudCover','pressure','Zenith','Azimuth','ETR','generation']]
df5=df5.interpolate()         
df5=df5.fillna(method='bfill')         

#split dataframe in test and train
split_date = pd.datetime(2017,12,31)

train = df5.loc[df5['datetime'] <= split_date]
test = df5.loc[df5['datetime'] > split_date]

x_train =train[['temperature', 'dewPoint', 'humidity', 'cloudCover','pressure','Zenith', 'Azimuth', 'ETR']]
x_test= test[['temperature', 'dewPoint', 'humidity', 'cloudCover','pressure','Zenith', 'Azimuth', 'ETR']]


y_train=train['generation']
y_test=test['generation']


from sklearn.neural_network import MLPRegressor
import numpy
numpy.random.seed(121)
clf = MLPRegressor(activation='relu', alpha=0.0001,learning_rate='constant',solver='lbfgs',shuffle=False)

clf.fit(x_train,y_train)

predicted= clf.predict(x_test)


dtxb= pd.DataFrame({'datetime':test['datetime'],'actual':y_test,'predictions':predicted})
dtxb.loc[dtxb.predictions<0,'predictions']=0
dtxb['error']=abs(dtxb['actual']-dtxb['predictions'])
dtxb['acc']=(1-dtxb['error']/30)*100
dtxb= dtxb[dtxb.actual>0]
mxb=dtxb['acc'].mean()

dtxb.to_csv('D:/climate connect/neural_network_report.csv')
 # accuracy is 94.25%


